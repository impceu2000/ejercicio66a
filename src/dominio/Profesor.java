package dominio;

import java.util.ArrayList;

public abstract class Profesor {
    ArrayList<Merito> listaMeritos = new ArrayList<>();

    public abstract double calcularValoracion();

    public void annadirMerito(Merito merito) {
        listaMeritos.add(merito);
    }

}
