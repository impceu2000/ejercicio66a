package dominio;

import java.util.ArrayList;

public class Catedratico extends Profesor {
    ArrayList<Merito> catedratico = new ArrayList<>();

    public void annadirMerito(Merito merito){
        catedratico.add(merito);
    }

    @Override
    public double calcularValoracion() {
        return 0;
    }
}
